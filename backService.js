
const express = require('express');
const app = express();
const request = require('request');
const cheerio = require('cheerio'); // Web Scraping
const sqlite3 = require('sqlite3').verbose();

const port=8080;
app.use(express.static('htmlapp'));

//////////////////////Temperature difference//////////////////////////
app.get ('/api/diff/000D6F0003141E14', function(req, res){
    const url= `http://wttr.in/Helsinki`;  // Website for Scraping
    var getTemperature = function(callback) {
        request.get (url, function (error, res, pre){

            if (error){
                return console.error(err.message);
            }
            else 
            {
                $ = cheerio.load(pre);   // Required data is located in "pre" -tag
                const spanData = $('span').children().remove().end();// Check and remove/ignore "child elements"
                const spanMin = $(spanData)[2];     // Get correct span data
                const spanMax = $(spanData)[3];
                const minVal = Number($(spanMin).text()); // Get text value and treat it as a number
                const maxVal = Number($(spanMax).text());
                var difference = minVal > maxVal ? (minVal - maxVal) : (maxVal - minVal); 
                return callback(difference) // Calculate the difference of 2 values and return it
            } 
        });
    };
    
    getTemperature(function (temperature)  
    { 
        res.setHeader('Content-Type', 'application/json');
        res.json({ "difference" : temperature}); // Send response as a JSON string
    });
});

//////////////////////Sensor data summary///////////////////
app.get ('/api/summary', function(req, res) {   // Open database as a "read only"
    let database = new sqlite3.Database('iot_db.sqlite', sqlite3.OPEN_READONLY , function(error) {
        if (error){
            return console.error(err.message);
        }
        else{
            return console.log('Connected to SQLite3 database');    
        }  
    });

    database.serialize(function() {
        var sensorParams= [];
        var sqlCommand = "SELECT SensorId, (Temperature/100.0) AS 'avgTemp', Battery AS 'count' FROM cubesensors_data";
        database.all(sqlCommand, function(error, allRows) { // Get data based on the "sqlCommand" -variable
             
            if(error != null){
                console.error(err.message);
            }
            else {
                allRows.forEach(function(row){  // Save row data in to an array
                    sensorParams.push(
                    {
                        "sensorId": row.SensorId,
                        "count": row.count, 
                        "avgTemp": row.avgTemp,
                    });
                });   
            }
            res.json({
                "sensors": sensorParams // Return array as a JSON 
            })

            database.close(function(error) {
                if (error){
                    console.error(error);
                }
                console.log ('Closing connection'); // Close database when response is finished
            });      
        });
    });
});
app.listen(port); // Listen port 8080